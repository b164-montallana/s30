const express = require("express");
//Mongoose is a package that allows creation of Schemas to model our data structure.
//Also has access to a number of method for manipulation of our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;
//MongoDB Atlast connection
//when we want to use local mongoDB/robo3t
//mongoose.connect("mongoDB://localhost:27017/databaseName")
mongoose.connect("mongodb+srv://admin:admin@firstproject.6tc50.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
let db = mongoose.connection;

//connection error message
db.on("error", console.error.bind(console, "connection error"));
//connection successful message
db.once("open", () => console.log("We're connected to the cloud database "))


app.use(express.json());

app.use(express.urlencoded({ extended: true}));

//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//act as blueprint to our data
//Use Schema() constructor of the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        //Default values
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema);

//user Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

const User = mongoose.model("User", userSchema);
//Routes/endpoints

//Creating a new task

//Business logic
/* 
1. Add a functionality to check if there are duplicates
    -if the task already exist in the database, we return error
    -if the task doesnt exist we add it in the database
        1. The task data will be coming from the request body
        2. Create a new task object with field/property
        3. save the new object to our database
*/

app.post("/tasks", (req, res) => {
    Task.findOne({ name: req.body.name }, (err, result) => {
        //if the document was found and the documents name matches the information sent via the client
        if(result != null && result.name === req.body.name){
            return res.send("Duplicate task found")
        } else {
        //if no document was found create a new task and save it to the database
            let newTask = new Task({ name: req.body.name });

            newTask.save((saveErr, saveTask) => {
                //if there are errors in saving 
                if(saveErr){
                    return console.log(saveErr)
                } else {
                    return res.status(201).send("New task created")
                }
            })
        }
    })
})

//Get all task
//Business logic
/* 
1. Find/retrieve all the documents
2. if an error is encountered, print the error
3. if no errors are found, send a success status back to the client and return an array of documents

*/
app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        if(err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                dataFromMDB: result
            })
        }
    })
})

// 3. Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup",(req, res) => {
    User.findOne({ username: req.body.username }, (err, result) => {
        if(result != null && result.username === req.body.username) {
            return res.send("Username already exist")
        } else {
            let newUser = new User ({ username: req.body.username, password: req.body.password });
            newUser.save((saveErr, saveUser) => {
                if(saveErr) {
                return console.log(saveErr)
                } else {
                    return res.status(201).send("New user created")
                }
            })
        }
    })
})

//5. Create a GET route that will return all users.
app.get("/users",(req,res) => {
    User.find({}, (err, result) => {
        if(err) {
            return console.log(err);
        } else {
            return res.status(200).json({usersData: result})
        }
    })
})







app.listen(port, () => console.log(`Server running at port ${port}`));
